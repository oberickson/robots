%w[sinatra dm-core dm-migrations slim sass].each{ |lib| require lib }

DataMapper.setup(:default, ENV['DATABASE_URL'] || File.join("sqlite3://",settings.root, "development.db"))

class Robot
  include DataMapper::Resource
  property :id,           Serial
  property :top,          Integer, :default =>  proc { 1+rand(6) }
  property :middle,       Integer, :default =>  proc { 1+rand(4) }
  property :bottom,       Integer, :default =>  proc { 1+rand(5) }
end

DataMapper.finalize

get '/' do
  @robots = Robot.all
  slim :index
end

post '/build/robot' do
  robot=Robot.create
  if request.xhr?
    slim :robot, { :layout=>false, :locals => { :robot => robot } }
  else
    redirect to('/')
  end
end

delete '/delete/robot/:id' do
  Robot.get(params[:id]).destroy
  redirect to('/') unless request.xhr?
end

__END__
########### Views ###########
@@layout
doctype html
html
  head
  meta charset="utf-8"
  title Robot Factory
  link rel="shortcut icon" href="/fav.ico"
  link href="http://fonts.googleapis.com/css?family=Megrim|Ubuntu&v2" rel='stylesheet'
  link rel="stylesheet" media="screen, projection" href="css/style.css"
  /[if lt IE 9]
    script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"
  body
    == yield
    footer role="contentinfo"
      p Building Quality Robots since 2011

@@index
h1 Robot Factory
form.build action="/build/robot" method="POST"
  input.button type="submit" value="Build A Robot!"
-if @robots.any?
  ul#robots
    - @robots.each do |robot|
      ==slim :robot, :locals => { :robot => robot }
- else
  h2 You Need To Build Some Robots!
  
@@robot
li.robot
  img src="https://s3.amazonaws.com/daz4126/top#{robot.top}.png"
  img src="https://s3.amazonaws.com/daz4126/middle#{robot.middle}.png"
  img src="https://s3.amazonaws.com/daz4126/bottom#{robot.bottom}.png"
  form.destroy action="/delete/robot/#{robot.id}" method="POST"
    input type="hidden" name="_method" value="DELETE"
    input type="submit" value="&times;"
